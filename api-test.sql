--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.5 (Debian 14.5-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: test
--

INSERT INTO public."SequelizeMeta" (name) VALUES ('20211222082515-members-view.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220131102625-add-invoice-type.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220131102632-add-invoice-audit-type.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220201110200-add-senderEmail-invoice-model.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220201110207-add-senderEmail-audit-invoice-model.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220202090256-add-columns-user-preferences.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220202113106-add-firstPageThumbnail-to-invoice-model.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220213083252-add-avatar-to-UserPreferences.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220215065331-add-BeneficiaryStatus.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220215071257-add-currency_code-beneficiary-model.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220215074023-update-currency-metadata-box.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220218120042-account-integration-id.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220221083006-supplier-integration-id.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220222145850-invoice-bill-id.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220228104655-invoice-exported-at.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220307144331-bank-account-columns.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220307095843-bank-account-columns.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220314105703-organization-preference-connected.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220322141244-organization-preferences-guide.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220325145602-not-null-payment-status.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220324085302-beneficiary-missing-columns.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220324090815-remove-bank-number-column.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220324111423-databox-metadata.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220324124549-databoxes-missing-columns.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220403065005-invoice-details-cols.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220403065259-remove-tax-number-from-beneficiary.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220403153612-invoice-payment-date.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220406073748-invoice-missing-asap-column.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220411053027-update-prefs-columns.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220424151054-account-supplier-deleted.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220510080914-account-supplier-unique-index.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220529132359-beneficiary-big-int.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220601095317-add-nick-name-to-beneficiary-accounts.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220609064705-add-quick-books-type-to-accounts.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220612042400-add-payment-method-to-invoices.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220612125349-add-last-sync-date-to-orgnization-preferences.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220614081651-add-tax-type-to-invoice-details.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220614081721-add-memo-to-invoice-details.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220614081730-add-description-to-invoice-details.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220614105902-add-currency-to-suppliers.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220615065413-add-online-exchange-rate-to-invoices.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220412113027-add-kyb-id-to-org.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220504102134-add-kyc-id-to-user-table.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220726103800-add-track-id-to-user-table.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220726131522-add-track-id-to-organization-table.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220807080626-update_user_track_id.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220807080730-update_org_track_id.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220812114737-change-kyc-id-column-string-to-users.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220812115555-remove-kyc-id-column-to-users.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20221018173728-update-token-in-push-notifications-table.js');


--
-- PostgreSQL database dump complete
--

