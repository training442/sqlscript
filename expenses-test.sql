--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.5 (Debian 14.5-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: test
--

INSERT INTO public."SequelizeMeta" (name) VALUES ('20220630074135-expense-has-no-receipts.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220703205109-vat-amount.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220703205158-expense-status-enum.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220725131745-expense-activity-columns.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220731093837-expense-item-decimals.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220818134230-add-exporting-info-to-expenses.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220902073748-add-payment-account-to-expenses.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220907202833-migration-rename-payment-account-add-memo-exported-id.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220916142222-migration-add-manager-id-to-expense-case.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20221018090059-remove-deleted-at-in-expense-tags.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220928105039-create-citext-extension.js');


--
-- PostgreSQL database dump complete
--

