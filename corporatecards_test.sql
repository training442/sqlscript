--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.5 (Debian 14.5-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: test
--

INSERT INTO public."SequelizeMeta" (name) VALUES ('20220726105432-add-card-color-to-card-table.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220727095851-add-active-since-and-issue-date-to-card-table.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220928105353-create-citext-extension.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20221016074921-kyc-status-unique-index.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20221023072123-add-parent-transaction-id.js');


--
-- PostgreSQL database dump complete
--

