--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.5 (Debian 14.5-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: test
--

INSERT INTO public."SequelizeMeta" (name) VALUES ('20220822053204-change-item-types-id-string-length.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220822063738-add-new-item-types.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220825091922-add-pemo-item-types.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220911095317-add-new-item-types.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20220927124940-all-item-types.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20221013072137-add-org-id-additional-items.js');
INSERT INTO public."SequelizeMeta" (name) VALUES ('20221009092042-create-citext-extension.js');


--
-- PostgreSQL database dump complete
--

